package lab11;
import java.util.HashMap;
import java.util.HashSet;


public class WordCounter {
	private String str;
	private HashMap<String,Integer> wordCount = new HashMap<String, Integer>();
	
	public WordCounter(String str){
	this.str = str;
	}
	
	public void count() {
		String[] s = this.str.split(" ");
		HashSet<String> str = new HashSet<String>();
		for(String a : s){
			str.add(a);
		}
		for(String b : str){
			int count = 0;
			for (int i = 0 ; i< s.length ; i++){
				if(b.equals(s[i])){
					count++;
				}
			}
			wordCount.put(b, count);
		}

		
	}
	
	public int hasWord(String word) {
		return wordCount.get(word);
	}
}